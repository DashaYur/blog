import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiGetterService } from "./api-getter.service";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

public posts: any[] = [];

  constructor(private api: ApiGetterService) { }

  ngOnInit(): void {
  this.api.getData('posts').subscribe((data) => {
  console.log(data);
  this.posts = data;
  })
  }

}
