import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiGetterService {
private apiRoot = 'http://jsonplaceholder.typicode.com/';

  constructor(private http: HttpClient) {}

  getData(endpoint: string) {
   return this.http.get(this.apiRoot + endpoint);
   }
}
